﻿Imports RabbitMQ.Client
Imports RabbitMQ.Client.Events
Imports SAPbobsCOM
Imports System.Text

Friend Module mq2sap
    Private oCompany As Company = New Company()

    Public Sub Main()
        MainScope()
        Console.ReadKey()
    End Sub

    Public Sub MainScope()
        Try
            '0. DI API Connection
            DIAPICoonection()
            '1. Connect to MQ
            Dim factory = New ConnectionFactory() With {
                .HostName = My.Settings.mqserver
            }
            '2. Send Request for Next Message from MQ
            Using connection = factory.CreateConnection()

                Using channel = connection.CreateModel()
                    channel.QueueDeclare(queue:=My.Settings.oc2mqdest, durable:=False, exclusive:=False, autoDelete:=False, arguments:=Nothing)
                    Dim consumer = New EventingBasicConsumer(channel)
                    '3. Receive Message from MQ
                    AddHandler consumer.Received, Function(model, ea)
                                                      Dim body = ea.Body
                                                      Dim message = Encoding.UTF8.GetString(body)
                                                      PrepareMSG(message)
                                                      Console.WriteLine(" [x] Recebido {0}", message)
                                                  End Function

                    channel.BasicConsume(queue:=My.Settings.oc2mqdest, autoAck:=True, consumer:=consumer)

                End Using
            End Using
        Catch ex As Exception
            Console.WriteLine("Erro on MainScope:" & ex.Message)
        End Try
    End Sub

    Public Function MQ2XML(message As String) As XElement
        Dim element As XElement = XElement.Parse(message)
        Return element
    End Function

    Public Sub PrepareMSG(message As String)
        Dim nErr As Long
        Dim errMsg As String
        Try
            Dim xmlDA As XElement
            '3.1. Prepare to Send to SAP
            xmlDA = MQ2XML(message)
            '3.2. Aloca os valores do XML nos elementos
            Dim DA As IEnumerable(Of XElement) = xmlDA.Elements()
            '4. Send to SAP
            If DIAPICoonection() Then
                '3.2. Salvar o Cliente    
                Dim oBPCliente As BusinessPartners = CType(oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners), BusinessPartners)

                'Escopo de recordset
                Dim RClientes As Recordset = CType(oCompany.GetBusinessObject(BoObjectTypes.BoRecordset), Recordset)
                RClientes.DoQuery("SELECT DfltSeries FROM ONNM WHERE ObjectCode = '2' AND DocSubType = 'C'")
                RClientes.MoveFirst()

                oBPCliente.Series = CInt(RClientes.Fields.Item("DfltSeries").Value.ToString())
                'oBPCliente.CardCode = "C"
                oBPCliente.CardName = DA("NOM_DOCTO_CLIENTE").Value
                oBPCliente.CardType = BoCardTypes.cCustomer
                'oBPCliente.CommissionGroupCode = 0
                'oBPCliente.CommissionPercent = 15
                oBPCliente.Company_Private = BoCardCompanyTypes.cPrivate
                'oBPCliente.ContactPerson = "C1"
                oBPCliente.Currency = "Real"
                oBPCliente.VatLiable = BoVatStatus.vLiable
                'oBPCliente.ShippingType = 3

                oBPCliente.Block = DA("NOM_BAIRRO").Value
                oBPCliente.ZipCode = DA("NUM_CEP").Value
                oBPCliente.City = DA("NOM_CIDADE").Value
                oBPCliente.Address = DA("END_LOGRADOURO").Value
                oBPCliente.Phone1 = DA("NUM_TELEFONE").Value
                oBPCliente.EmailAddress = DA("NOM_END_EMAIL").Value


                If (oBPCliente.Add() <> 0) Then
                    Console.WriteLine("Falha no cadastro de Clientes.")
                Else
                    Console.WriteLine("¨Novo cliente registrado, novo objcode=" & oBPCliente.CardCode)
                    'Salva os dados Fiscais do Cliente
                    Dim oBPClienteFiscal As BPFiscalTaxID = CType(oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners), BPFiscalTaxID)
                    oBPClienteFiscal.TaxId0 = DA("NUM_DOCTO_CLIENTE").Value
                    oBPClienteFiscal.Add()

                End If



                '3.3. Salva lançamento contábil
                Dim oJournalEntry As JournalEntries = CType(oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries), JournalEntries)
                oJournalEntry.DocumentType = ""
                oJournalEntry.Memo = DA("DSC_SERVICO").Value
                oJournalEntry.ProjectCode = ""

                oJournalEntry.TaxDate = Now
                oJournalEntry.Lines.AccountCode = "1.01.03.01.01"
                oJournalEntry.Lines.ContraAccount = "1.01.01.02.01"
                oJournalEntry.Lines.Credit = DA("VAL_TOTAL_DA").Value
                oJournalEntry.Lines.Debit = 0
                oJournalEntry.Lines.DueDate = CDate(DA("DAT_VENCTO").Value)
                oJournalEntry.Lines.ReferenceDate1 = Now
                oJournalEntry.Lines.ShortName = DA("NOM_DOCTO_CLIENTE").Value
                oJournalEntry.Lines.TaxDate = Now
                oJournalEntry.Lines.Add()
                oJournalEntry.Lines.SetCurrentLine(1)

                oJournalEntry.Lines.AccountCode = "1.01.01.02.01"
                oJournalEntry.Lines.ContraAccount = DA("NOM_DOCTO_CLIENTE").Value
                oJournalEntry.Lines.Credit = 0
                oJournalEntry.Lines.Debit = DA("VAL_TOTAL_DA").Value
                oJournalEntry.Lines.DueDate = CDate(DA("DAT_VENCTO").Value)
                oJournalEntry.Lines.ReferenceDate1 = Now
                oJournalEntry.Lines.ShortName = "1.01.01.02.01"
                oJournalEntry.Lines.TaxDate = Now
                'Define a Filial 
                oJournalEntry.Lines.BPLID = DA("FILIAL").Value

                If (oJournalEntry.Add() <> 0) Then
                    Console.WriteLine("Falha ao inserir o lançamento contábil no SAP.")
                Else
                    Console.WriteLine("LC criado com sucesso, Nro:" & (oJournalEntry.JdtNum))
                End If
            End If


            oCompany.Disconnect()
            oCompany = Nothing
        Catch ex As Exception
            oCompany.GetLastError(nErr, errMsg)
            Console.WriteLine("Erro ao enviar ao SAP:" + Err.Description)

        End Try
    End Sub


    Function DIAPICoonection() As Boolean
        oCompany.Server = My.Settings.DIServer
        oCompany.language = BoSuppLangs.ln_Portuguese_Br
        oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016
        oCompany.DbUserName = My.Settings.DIDbUserName
        oCompany.DbPassword = My.Settings.DIDbPassword
        oCompany.CompanyDB = My.Settings.DICompanyDB
        oCompany.UserName = My.Settings.DIUserName
        oCompany.Password = My.Settings.DIPassword
        If Not oCompany.Connect() Then ' 0 - Connected
            Console.WriteLine(" Conectado ao SAP com Sucesso!")
            Return True
        Else
            Console.WriteLine("Erro ao conectar no SAP:" & oCompany.GetLastErrorDescription())
            Return False
        End If

    End Function
End Module
