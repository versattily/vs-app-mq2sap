﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' As informações gerais sobre um assembly são controladas por
' conjunto de atributos. Altere estes valores de atributo para modificar as informações
' associada a um assembly.

' Revise os valores dos atributos do assembly

<Assembly: AssemblyTitle("vs-app-mq2sap")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Microsoft")>
<Assembly: AssemblyProduct("vs-app-mq2sap")>
<Assembly: AssemblyCopyright("Copyright © Microsoft 2018")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'O GUID a seguir será destinado à ID de typelib se este projeto for exposto para COM
<Assembly: Guid("5f80a2c7-1505-46da-bf7e-638a220cd1ce")>

' As informações da versão de um assembly consistem nos quatro valores a seguir:
'
'      Versão Principal
'      Versão Secundária 
'      Número da Versão
'      Revisão
'
' É possível especificar todos os valores ou usar como padrão os Números de Build e da Revisão
' utilizando o "*" como mostrado abaixo:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
